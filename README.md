# SUMMARY STEPS

- PostgreSQL version >= 9.6*
- Prepare The Target Database
- Install Forgeign Data Wrapper, tds_fdw
- Identify Foreign Table connection info
- Identify System Catalogs and Tables of Interest
    - Functions
    - Tables
    - Constraints: PK, FK, CHECK
    - Views
    - Triggers
- Process Functions/sprocs
- Create Foreign Table(s)
- Copy FT data into PostgreSQL
- Recreate table schema
- Populate table(s)
- Apply Constraints
- Process Triggers
- Process Views (an iterative LIFE-CYCLE approach)

## THE SECRET SAUCE TO A SUCCESSFULL DATA MIGRATION
    There's more than one way to "Skin A Cat"

- MSSQL
    - ANSI compliant
    - It possesses an "information_schema"
    - The schema "sys" is equivalent to the postgres "pg_catalog" schema
- Overloading Operator and Datatype conversions simplifies the task
- Recognize that importing functions/sprocs is tedious work
- Recognize the need to be able to rebuild your environment over and over again
- Get yourself a dependable, linux based, CLI mssql client

## INSTALLING POSTGRES

Update your repository:
```
    echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list
```
Import the repository signing key:
```
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
```
Update the package lists:
```
    apt-get update
    apt-get dist-upgrade -y
```
Install PostgreSQL
```
    apt-get install -y postgresql-contrib-9.6 postgresql-server-dev-9.6
```
## PREPARING THE TARGET DATABASE

CREATE YOUR DATABASE, SCHEMA
- schema layout
    - public,                   -- default postgres
    - dbo,                      -- default MSSQL
    - mssql_information_schema, -- foreign tables, information_schema
    - mssql,                    -- foreign tables
    - run_information_schema,   -- imported tables, information_schema
    - run,                      -- imported tables
    - views,                    -- regenerated views

TIP: set the search_path
```
    alter database <mydatabase> set search_path=public,run,views,run_information_schema,mssql,mssql_information_schema,dbo;
```
## THE tds_fdw FOREIGN DATA WRAPPER

Install necesary, dependant packages on your OS
```
    apt-get -y install make gcc git libsybdb5 freetds-dev freetds-common
```
Get the foreign data wrapper for MSSQL
```
    git clone https://github.com/tds-fdw/tds_fdw.git
```
Install the foreign data wrapper (assumes pg_config is in PATH)
```
    cd tds_fdw
    make USE_PGXS=1 install
```
Create Extension
```
    createdb <mydatabase>;
    psql <mydatabase> -c 'create extension if not exists tds_fdw with schema public'
```
## CONNECTING PG AND MSSQL
```
/* -- EX: Variables, file:conn.info --

USER=<postgres ROLE>

MSSQL_SERVER=<MSSQL FQDN>
MSSQL_PORT='1433'
MSSQL_DB=<MSSQL database name>
MSSQL_USER=<MSSQL database username>
MSSQL_PASS=<MSSQL password>

*/
```
-- NOTICE the use of variable names
--
```
create server mssql_svr
    foreign data wrapper tds_fdw
    options (servername 'MSSQL_SERVER',
            port 'MSSQL_PORT',
            database 'MSSQL_DB',
            tds_version '7.3',
            msg_handler 'notice');

create user mapping for $USER
    server mssql_svr
    options (username 'MSSQL_USER', password 'MSSQL_PASS');
```
-- secret sauce
--
```
import foreign schema dbo
    from server mssql_svr
    into mssql
    options (import_default 'true');
```
## IMPORTING TABLES FROM MSSQL
```
IMPORT FOREIGN SCHEMA remote_schema
      [ { LIMIT TO | EXCEPT } ( table_name [, ...] ) ]
      FROM SERVER server_name
      INTO local_schema
      [ OPTIONS ( option 'value' [, ... ] ) ]
```
EXAMPLE: (based upon a prexisting set of definitions)
  DATABASE: temp
    SCHEMA: dbo
    SERVER: mssql_svr
```
  import foreign schema dbo
    from server mssql_svr
    into public
    options (import_default 'true');
```
EXAMPLE:
```
  create table t1_new (like t1);
```

NOTES:
- One can create a table based upon the schema of the foreign table:
- Either "create foreign table" and "import foreign schema" works.

## PROCESSING FUNCTIONS/SPROCS
    (this is kind of fuzzy, welcome to cowboy land)

- issues:
    - about datatypes
    - about operators
    - function overloading, mismatches
    - steps
        - CHECK constraints in tables
        - reproduce with equivalent IN and OUT arguments, no code
        - implement/debug the code

TIP: don't bother tweeking the tables, instead overload your operators and datatypes... then fix your tables

## IMPORT TABLE SCHEMA
```
script: 01.create_FT_mssql.sh
```
CAVEATS:
- Casting Data Types
- Column and Table mixed-case names

## POPULATE TABLES
```
script: 02.populate_tables_mssql.sh
```
## IMPORT SYSTEM CATALOGS
    REQUIRED FOR CONSTRAINTS AND VIEWS
```
script: 03.create_FT_information_schema.sh
```

```
IMPORTING THE FOLLOWING:
    information_schema.constraint_column_usage
    information_schema.table_constraints
    information_schema.referential_constraints
    information_schema.key_column_usage
    information_schema.views
    information_schema.view_table_usage
    information_schema.view_column_usage
    information_schema.tables
    sys.objects
    sys.sql_modules
    sys.syscolumns
    sys.sysobject
```
ATTENTION: Do not trust the existing documentation! Test/Query the tables in MSSQL "before" importing them.

## POPULATE FROM SYSTEM CATALOGS
```
script: 04.populate_tables_information_schema.sh
```
## IMPORT THE CONSTRAINTS: PK, FK
```
script: 05.add_primary_keys.sh
        06.add_foreign_keys.sh
```
## PROCESS THE TRIGGERS
```
script: 07.get_trigger_lists.sh
```
- generate a list: take advantage of your mssql client i.e. execute custom query (cheat: google for it!)
- output MSSQL trigger definition to individual text files
- process/edit each individual MSSQL definition
    - PG issues:
        - function definition
        - trigger definition

TIPS:
- Figure out and leverage shortcuts where possible
- Alterrnatively, one can import trigger definitions using the Foreign Data Wrapper

## PROCESS THE VIEWS
###    LIFE-CYCLE

```
script: 08a.add_views.sh
        08b.regenerate_views.sh
        08c.get_problem_views.sh
```

- Identify those MSSQL system catalogs pertaining to views
- Create Foreign Tables
- Copy view definitions into target tables using an ETL process
- Create table containg meta data of failed SQL create view
- VIEW CREATION CYCLE: life cycle of success/failure of view creation
    - execute a single pass through all view definitions
    - execute view SQL: upon view create failure, register information into meta table
    - repeat previous steps: continue until you've confirmed no new views are being created
    - refering to meta-table, execute SQL and output all failed view definitions as individual SQL files
    - edit view definitions
    - update view catalog with updated definitions
    - repeat entire VIEW CREATION CYCLE

TIPS:
- It is understood that each successive pass of the VIEW CREATION CYCLE attempts to create views only previously failed views.
- As you edit view definitions; focus on those views with other error messages such as missing, or incorrect functions, improper casting etc. Leave those that message missing views or tables for last.
- Remember, debugging and validating views is an entirely different exercise :-)
