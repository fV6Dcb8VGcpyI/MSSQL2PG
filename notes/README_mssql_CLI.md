# INSTALLING sql-cli ON CENTOS 6
## THE FOLLOWING IS A ROUGH AND READY SET OF NOTES GETTING THE "SQL-CLI" CLI SETUP
```
# INSTALL nodejs
wget https://nodejs.org/dist/v6.9.1/node-v6.9.1-linux-x64.tar.xz
unxz node-v6.9.1-linux-x64.tar.xz
tar -xpvf node-v6.9.1-linux-x64.tar
--------------------------------
# INSTALL sql-cli
export PATH=<mypath>/node-v6.9.1-linux-x64/bin:$PATH
yum install git
--------------------------------
git clone https://github.com/hasankhan/sql-cli
cd sql-cli
git checkout dev
npm install -g
```
--------------------------------
# EX INVOCATION

```
$mssql --help

  Usage: mssql [options]

  Options:

    -h, --help                      output usage information
    -V, --version                   output the version number
    -s, --server <server>           Server to conect to
    -u, --user <user>               User name to use for authentication
    -p, --pass <pass>               Password to use for authentication
    -o, --port <port>               Port to connect to
    -t, --timeout <timeout>         Connection timeout in ms
    -T, --requestTimeout <timeout>  Request timeout in ms
    -d, --database <database>       Database to connect to
    -q, --query <query>             The query to execute
    -v, --tdsVersion <tdsVersion>   Version of tds protocol to use [7_4, 7_2, 7_3_A, 7_3_B, 7_4]
    -e, --encrypt                   Enable encryption
    -f, --format <format>           The format of output [table, csv, xml, json]
    -c, --config <path>             Read connection information from config file
```

Example:
```
mssql -s <HOST> -u <USER> -p <PASSWORD> -d <DATABASE>
```

```
mssql> .help
command description
-------------- ------------------------------------------
.help Shows this message
.tables Lists all the tables
.databases Lists all the databases
.read FILENAME Execute commands in a file
.run FILENAME Execute the file as a sql script
.schema TABLE Shows the schema of a table
.indexes TABLE Lists all the indexes of a table
.analyze Analyzes the database for missing indexes.
.quit Exit the cli
```
=======================================================
## REFERENCES:
##  https://www.npmjs.com/package/sql-cli
