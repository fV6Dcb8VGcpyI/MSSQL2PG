# ABOUT MSSQL2PG VIEWS AND ITS CREATION/EDIT MIGRATION, DEVELOPMENT LIFE-CYCLE

The following information is also found elsewhere in the various shell scripts.

```
-- This is the table containing VIEW ERROR meta-data.
--
    create table run_information_schema.vviews_update (
         view_schema             information_schema.sql_identifier not null default 'views'
        ,view_name               information_schema.sql_identifier not null
        ,view_definition         information_schema.character_data not null
        ,view_definition_md5     uuid not null
        ,view_err                text not null
        ,view_err_context        text not null
        ,view_sql_fixed          text
        ,is_compound             boolean not null default false
        ,is_fixed                boolean not null default false
        ,t_stamp                 timestamp with time zone default now()
        ,primary key (view_schema,view_name,view_definition_md5)
    );
```
