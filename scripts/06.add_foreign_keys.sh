#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
SQL1="
create or replace view run_information_schema.aa
as
with h as (select   table_name,
                    constraint_name,
                    column_name,
                    ordinal_position
            from run_information_schema.key_column_usage
            order by table_name,constraint_name,ordinal_position),
     a as (select constraint_name,
                  table_name,
                  constraint_type
                from run_information_schema.table_constraints
                where constraint_type='FOREIGN KEY'),
     b as (select table_name,
                   constraint_name,
                   string_agg(column_name,',') as column_name,
                   string_agg(ordinal_position::text,',') as ordinal_position
                from h
                join a using (table_name,constraint_name)
                group by table_name, constraint_name),
      c as (select b.*, bb.unique_constraint_name
                from b
                join run_information_schema.referential_constraints bb using (constraint_name)),
      d as (select table_name as foreign_table,
                   constraint_name as unique_constraint_name,
                   string_agg(column_name,',') as foreign_column,
                   string_agg(ordinal_position::text,',') as foreign_ordinal_position
                from h
                group by table_name, constraint_name)
select c.*,
       foreign_table,
       foreign_column,
       foreign_ordinal_position
from c join d using (unique_constraint_name);
"

SQL2="
DO
\$\$
DECLARE
    sql text;
    r record;
BEGIN
    SET client_min_messages=notice;
    SET search_path=run,run_information_schema;

    FOR r IN select table_name,
                    column_name,
                    foreign_table,
                    foreign_column
              from run_information_schema.aa
    LOOP
        sql = 'alter table '
            || r.table_name
            || ' add foreign key'
            || '(' || r.column_name
            || ') references '
            || r.foreign_table
            || '('
            || r.foreign_column
            || ')';

        RAISE NOTICE '%', sql;
        EXECUTE sql;
    END LOOP;
END
\$\$;
"

echo "$SQL1" | psql --set ON_ERROR_STOP=on "$CONN"
echo "$SQL2" | psql --set ON_ERROR_STOP=on "$CONN"
echo "$(date): DONE"
