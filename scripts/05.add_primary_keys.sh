#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
SQL1="
DO
\$\$
DECLARE
    tbl text;
    col  text;
    SQL text;

BEGIN
    SET client_min_messages=notice;

    FOR tbl, col IN with a as (select table_name, constraint_name
                               from run_information_schema.table_constraints
                               where constraint_type='PRIMARY KEY')
                    select distinct on (b.table_name)
                           b.table_name,
                           string_agg(b.column_name,',') as primary_key
                    from run_information_schema.constraint_column_usage b join a using (constraint_name)
                    group by b.table_name
    LOOP
        RAISE NOTICE 'PRIMARY KEY FOR TABLE: %',tbl;
        SQL = 'alter table run.' || tbl || ' add primary key (' || col || ')';
        EXECUTE SQL;
    END LOOP;
END
\$\$;
"

SQL2="
DO
\$\$
DECLARE
    tbl text;
    col  text;
    SQL text;

BEGIN
    SET client_min_messages=notice;

    FOR tbl, col IN with a as (select table_name, constraint_name
                               from run_information_schema.table_constraints
                               where constraint_type='UNIQUE')
                    select distinct on (b.table_name)
                           b.table_name,
                           string_agg(b.column_name,',') as unique_key
                    from run_information_schema.constraint_column_usage b join a using (constraint_name)
                    group by b.table_name
    LOOP
        RAISE NOTICE 'UNIQUE FOR TABLE: %',tbl;
        SQL = 'alter table run.' || tbl || ' add unique (' || col || ')';
        EXECUTE SQL;
    END LOOP;
END
\$\$;
"

echo "$SQL1" | psql --set ON_ERROR_STOP=on "$CONN"
echo "$SQL2" | psql --set ON_ERROR_STOP=on "$CONN"

echo 'performing ANALYZE ...'
echo "analyze" | psql --set ON_ERROR_STOP=on "$CONN"
echo "$(date): DONE"
