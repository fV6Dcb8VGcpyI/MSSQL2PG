#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
psql "$CONN" <<_eof_
    \set ON_ERROR_STOP on
     truncate run_information_schema.vviews_update;
     drop schema if exists views cascade;
     set search_path=views,public,run,run_information_schema,mssql,mssql_information_schema,dbo;
     create schema views;

    \i inc/add_views1.sql
    \i inc/add_views2.sql
_eof_

echo "$(date): DONE"
