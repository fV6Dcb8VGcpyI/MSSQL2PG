#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
SQL1="
SELECT
     sysobjects.name AS trigger_name
    ,OBJECT_NAME(parent_obj) AS table_name
    ,OBJECTPROPERTY( id, 'ExecIsUpdateTrigger') AS isupdate
    ,OBJECTPROPERTY( id, 'ExecIsDeleteTrigger') AS isdelete
    ,OBJECTPROPERTY( id, 'ExecIsInsertTrigger') AS isinsert
FROM sysobjects
INNER JOIN sys.tables t
    ON sysobjects.parent_obj = t.object_id
INNER JOIN sys.schemas s
    ON t.schema_id = s.schema_id
WHERE sysobjects.type = 'TR'
"

SQL2="
SELECT      c.[text]
FROM        sys.objects AS o
INNER JOIN  sys.syscomments AS c
ON      o.object_id = c.id
WHERE   o.[type] = 'TR'
"

mssql -s $MSSQL_SERVER -u $MSSQL_USER -p $MSSQL_PASS -d $MSSQL_DB -q "$SQL1" > triggers_list.$(date --rfc-3339=date).txt
mssql -s $MSSQL_SERVER -u $MSSQL_USER -p $MSSQL_PASS -d $MSSQL_DB -q "$SQL2" > triggers_body.$(date --rfc-3339=date).txt

echo "DONE"
