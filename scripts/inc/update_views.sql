\qecho *** Regenerating Missing Views *** ...

DO
$$
DECLARE
    r record;
    SQL text;
BEGIN
    SET client_min_messages='notice';

    CREATE TEMP TABLE IF NOT EXISTS v_list_tmp (view_name)
    AS
    SELECT lower(view_name)
        FROM run_information_schema.vviews_update
        WHERE view_name NOT IN (select viewname from pg_views where schemaname='views')
        ORDER BY 1;

    TRUNCATE run_information_schema.vviews_update;

    FOR r IN select view_name, view_definition
                from run_information_schema.vviews
                join v_list_tmp using (view_name)
    LOOP
    RAISE NOTICE '%', r.view_name;
        DECLARE
            err1 text;
            err2 text;
            err3 text;
            err4 text;
        BEGIN
            SQL = r.view_definition;
            RAISE NOTICE '%',r.view_name;
            EXECUTE SQL;
        EXCEPTION WHEN OTHERS THEN
          RAISE NOTICE 'ERROR: % cannot be created ...',r.view_name;

          GET STACKED DIAGNOSTICS err1 = MESSAGE_TEXT,
                                  err2 = PG_EXCEPTION_DETAIL,
                                  err3 = PG_EXCEPTION_HINT,
                                  err4 = PG_EXCEPTION_CONTEXT;

          begin
            insert into run_information_schema.vviews_update (
                view_schema, view_name, view_definition,
                view_definition_md5, view_err, view_err_context,
                view_sql_fixed, is_compound, is_fixed, t_stamp)

            values (
                DEFAULT,
                r.view_name,
                r.view_definition,
                md5(r.view_definition)::uuid,
                err1 || ' ' || err2 || ' ' || err3,
                err4,
                NULL,
                DEFAULT,
                DEFAULT,
                DEFAULT );

           exception when unique_violation then
            raise notice 'WARNING: % already exists in table VVIEWS_UPDATE',r.view_name;
           end;
        END;
    END LOOP;
END
$$;
