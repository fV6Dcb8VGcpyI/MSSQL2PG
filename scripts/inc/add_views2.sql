\qecho *** Adding Views Depending Upon Other Views *** ...

DO
$$
DECLARE
    r record;
    SQL text;
BEGIN
    SET client_min_messages='notice';

    FOR r IN with a as (select table_name
                            from run_information_schema.views
                            where table_name not like 'ctsv_%'
                            and table_name not like 'tsvw_%'
                            and substring(view_definition from 60) ~* 'vw_')
              select view_name, view_definition
              from run_information_schema.vviews
              where view_name in (select table_name from a)
    LOOP
        DECLARE
            err1 text;
            err2 text;
            err3 text;
            err4 text;
        BEGIN
            SQL = r.view_definition;
            RAISE NOTICE '% (Compound View)',r.view_name;
            EXECUTE SQL;
        EXCEPTION WHEN OTHERS THEN
          RAISE NOTICE 'ERROR: % (Compound View) cannot be created ...',r.view_name;

          GET STACKED DIAGNOSTICS err1 = MESSAGE_TEXT,
                                  err2 = PG_EXCEPTION_DETAIL,
                                  err3 = PG_EXCEPTION_HINT,
                                  err4 = PG_EXCEPTION_CONTEXT;

          begin
            insert into run_information_schema.vviews_update (
                view_schema, view_name, view_definition,
                view_definition_md5, view_err, view_err_context,
                view_sql_fixed, is_compound, is_fixed, t_stamp)

            values (
                DEFAULT,
                r.view_name,
                r.view_definition,
                md5(r.view_definition)::uuid,
                err1 || ' ' || err2 || ' ' || err3,
                err4,
                NULL,
                true,
                DEFAULT,
                DEFAULT );

           exception when unique_violation then
            -- do nothing, sort of
            raise notice 'WARNING: %, Compound View, already exists in table VVIEWS_UPDATE',r.view_name;
           end;
        END;
    END LOOP;
END
$$;
