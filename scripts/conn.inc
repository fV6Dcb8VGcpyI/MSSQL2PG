## EDIT THIS FILE ##

###############################################################
# CONNECTIVITY TO THE POSTGRES RDBMS
#
HOST='myhost'
DB='mydb'
USER='myuser'
PASSWORD='mypassword'

CONNpg="host=$HOST port=5432 dbname=postgres user=postgres password=$PASSWORD"
CONN="host=$HOST port=5432 dbname=$DB user=$USER password=$PASSWORD"
#
###############################################################
#  FOREIGN TABLE CONNECTIVITY FROM POSTGRES TO MSSQL
#
MSSQL_SERVER='my_mssql_host'
MSSQL_PORT='1433'
MSSQL_DB='my_mssql_db'
MSSQL_USER='my_mssql_user'
MSSQL_PASS='my_mssql_password'
#
###############################################################
