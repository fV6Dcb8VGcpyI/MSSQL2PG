#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
SQL="select relname
        from pg_class a join pg_namespace b on a.relnamespace=b.oid
        where b.nspname='mssql'
        order by 1"

LIST=$(echo $SQL | psql --set ON_ERROR_STOP=on -At "$CONN")

for u in $LIST
do
    echo "========= $u -- $(date) ============"
    sql="insert into run.$u select * from mssql.\"$u\";"
    echo "$sql" | psql --set ON_ERROR_STOP=on "$CONN"
done

echo "$(date): DONE"
