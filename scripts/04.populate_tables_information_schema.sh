#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
SQL1="select relname
        from pg_class a join pg_namespace b on a.relnamespace=b.oid
        where b.nspname='mssql_information_schema'
        order by 1"

LIST=$(echo $SQL1 | psql -1 --set ON_ERROR_STOP=on -At "$CONN")

for u in $LIST
do
    echo "========= $u: $(date) ============"
    sql="insert into run_information_schema.$u select * from mssql_information_schema.\"$u\";"
    echo "$sql" | psql --set ON_ERROR_STOP=on "$CONN"
done

