#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################

SQL="select view_name from vviews_update order by 1"

rm -f *.sql

for u in $(echo "$SQL" | psql -t --set ON_ERROR_STOP=on "$CONN")
do
    echo "processing $u ..."
    sql="select view_definition from run_information_schema.vviews_update  where view_name='$u'"
    echo "$sql" | psql -qtA --set ON_ERROR_STOP=on "$CONN" > $u.sql
done

echo "DONE"
