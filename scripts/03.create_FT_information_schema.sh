#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
SQL="
DO
\$\$
DECLARE
    tbl text;
    sql text;
BEGIN
    FOR tbl IN select relname
                from pg_class a join pg_namespace b on a.relnamespace=b.oid
                where b.nspname = 'mssql_information_schema'
    LOOP
        sql = 'create table run_information_schema.' || tbl || '(like mssql_information_schema.\"' || tbl || '\")';
        EXECUTE sql;

        sql = 'update pg_attribute set attname=lower(attname) where attname !=lower(attname)';
        EXECUTE sql;
    END LOOP;
END
\$\$;
"


psql "$CONN" <<_eof_

\set STOP_ON_ERROR on
\qecho *** Recreate INFORMATION SCHEMA Environment *** ...

    drop schema if exists mssql_information_schema, run_information_schema cascade;

    create schema mssql_information_schema;
    create schema run_information_schema;

\qecho *** Now Importing Foreign Tables *** ...

    create foreign table if not exists mssql_information_schema.constraint_column_usage (
        table_catalog information_schema.sql_identifier,
        table_schema information_schema.sql_identifier,
        table_name information_schema.sql_identifier,
        column_name information_schema.sql_identifier,
        constraint_catalog information_schema.sql_identifier,
        constraint_schema information_schema.sql_identifier,
        constraint_name information_schema.sql_identifier
    ) server mssql_svr
    options (table_name 'information_schema.constraint_column_usage', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.table_constraints (
        constraint_catalog information_schema.sql_identifier,
        constraint_schema information_schema.sql_identifier,
        constraint_name information_schema.sql_identifier,
        table_catalog information_schema.sql_identifier,
        table_schema information_schema.sql_identifier,
        table_name information_schema.sql_identifier,
        constraint_type information_schema.character_data,
        is_deferrable information_schema.yes_or_no,
        initially_deferred information_schema.yes_or_no
    ) server mssql_svr
    options (table_name 'information_schema.table_constraints', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.referential_constraints (
        constraint_catalog information_schema.sql_identifier,
        constraint_schema information_schema.sql_identifier,
        constraint_name information_schema.sql_identifier,
        unique_constraint_catalog information_schema.sql_identifier,
        unique_constraint_schema information_schema.sql_identifier,
        unique_constraint_name information_schema.sql_identifier,
        match_option information_schema.sql_identifier,
        update_rule information_schema.sql_identifier,
        delete_rule information_schema.sql_identifier
        ) server mssql_svr
    options (table_name 'information_schema.referential_constraints', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.key_column_usage (
        constraint_catalog information_schema.sql_identifier,
        constraint_schema information_schema.sql_identifier,
        constraint_name information_schema.sql_identifier,
        table_catalog information_schema.sql_identifier,
        table_schema information_schema.sql_identifier,
        table_name information_schema.sql_identifier,
        column_name information_schema.sql_identifier,
        ordinal_position information_schema.cardinal_number
    ) server mssql_svr
    options (table_name 'information_schema.key_column_usage', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.views (
        table_catalog information_schema.sql_identifier,
        table_schema information_schema.sql_identifier,
        table_name information_schema.sql_identifier,
        view_definition information_schema.character_data,
        check_option information_schema.character_data,
        is_updatable information_schema.yes_or_no
    ) server mssql_svr
    options (table_name 'information_schema.views', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.view_table_usage (
        view_catalog information_schema.sql_identifier,
        view_schema information_schema.sql_identifier,
        view_name information_schema.sql_identifier,
        table_catalog information_schema.sql_identifier,
        table_schema information_schema.sql_identifier,
        table_name information_schema.sql_identifier
    ) server mssql_svr
    options (table_name 'information_schema.view_table_usage', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.view_column_usage (
        view_catalog information_schema.sql_identifier,
        view_schema information_schema.sql_identifier,
        view_name information_schema.sql_identifier,
        table_catalog information_schema.sql_identifier,
        table_schema information_schema.sql_identifier,
        table_name information_schema.sql_identifier,
        column_name information_schema.sql_identifier
    ) server mssql_svr
    options (table_name 'information_schema.view_column_usage', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.tables (
        table_catalog information_schema.sql_identifier,
        table_schema information_schema.sql_identifier,
        table_name information_schema.sql_identifier,
        table_type information_schema.sql_identifier
    ) server mssql_svr
    options (table_name 'information_schema.tables', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.objects (
        name information_schema.sql_identifier,
        object_id information_schema.sql_identifier,
        principal_id information_schema.sql_identifier,
        schema_id information_schema.sql_identifier,
        parent_object_id information_schema.sql_identifier,
        type information_schema.sql_identifier,
        type_desc information_schema.sql_identifier,
        create_date information_schema.sql_identifier,
        modify_date information_schema.sql_identifier,
        is_ms_shipped information_schema.sql_identifier,
        is_published information_schema.sql_identifier,
        is_schema_published information_schema.sql_identifier
    ) server mssql_svr
    options (table_name 'sys.objects', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.sql_modules (
        object_id information_schema.sql_identifier,
        definition information_schema.sql_identifier,
        uses_ansi_nulls information_schema.sql_identifier,
        uses_quoted_identifier information_schema.sql_identifier,
        is_schema_bound information_schema.sql_identifier,
        uses_database_collation information_schema.sql_identifier,
        is_recompiled information_schema.sql_identifier,
        null_on_null_input information_schema.sql_identifier,
        execute_as_principal_id information_schema.sql_identifier,
        uses_native_compilation information_schema.sql_identifier
    ) server mssql_svr
    options (table_name 'sys.sql_modules', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.syscolumns (
      name      varchar(64),
      id        integer,
      xtype     smallint,
      typestat  smallint,
      xusertype smallint,
      length    smallint,
      xprec     smallint,
      xscale    smallint,
      colid     smallint,
      xoffset   smallint,
      bitpos    smallint,
      reserved  smallint,
      colstat   smallint,
      cdefault  integer,
      domain    smallint,
      number    smallint,
      colorder  smallint,
      autoval   smallint,
      "offset"  smallint,
      collationid integer,
      language  smallint,
      status    smallint,
      type      smallint,
      usertype  smallint,
      printfmt  smallint,
      prec      smallint,
      scale     smallint,
      iscomputed boolean,
      isoutparam boolean,
      isnullable boolean,
      "collation" varchar(64),
      tdscollation bytea
    ) server mssql_svr
    options (table_name 'sys.syscolumns', row_estimate_method 'showplan_all');

    create foreign table if not exists mssql_information_schema.sysobjects (
        name            varchar(128),
        id              integer,
        xtype           varchar(2),
        uid             smallint,
        info            smallint,
        status          smallint,
        base_schema_ver smallint,
        replinfo        smallint,
        parent_obj      integer,
        crdate          timestamp without time zone,
        ftcatid         smallint,
        schema_ver      smallint,
        stats_schema_ver smallint,
        type            varchar(2),
        userstat        smallint,
        sysstat         smallint,
        indexdel        smallint,
        refdate         timestamp without time zone,
        version         smallint,
        deltrig         integer,
        instrig         integer,
        updtrig         integer,
        seltrig         integer,
        category        smallint,
        cache           smallint
    ) server mssql_svr
    options (table_name 'sys.sysobjects', row_estimate_method 'showplan_all');

\qecho *** Now Creating Tables Based Upon Mssql Foreign Tables *** ...
    $SQL


\qecho *** Now Creating Table Retaining Information On Problem Views ***

    create table run_information_schema.vviews_update (
         view_schema             information_schema.sql_identifier not null default 'views'
        ,view_name               information_schema.sql_identifier not null
        ,view_definition         information_schema.character_data not null
        ,view_definition_md5     uuid not null
        ,view_err                text not null
        ,view_err_context        text not null
        ,view_sql_fixed          text
        ,is_compound             boolean not null default false
        ,is_fixed                boolean not null default false
        ,t_stamp                 timestamp with time zone default now()
        ,primary key (view_schema,view_name,view_definition_md5)
    );

_eof_

echo "$(date): DONE"
