#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
psql "$CONN" <<_eof_
       set search_path=views,public,run,run_information_schema,mssql,mssql_information_schema,dbo;
    \i inc/update_views.sql
_eof_

echo "$(date): DONE"
