#!/bin/bash

set -e

###################################################################
# CONNECTIVITY ENVINRONMENT VARIABLES
# REVIEW AND EDIT AS REQUIRED PRIOR TO EXECUTION

source conn.inc

#
###################################################################

############DO NOT EDIT BELOW THIS LINE ###########################
SQL="
DO
\$\$
DECLARE
    tbl text;
    sql text;
BEGIN
    FOR tbl IN select relname
                from pg_class a join pg_namespace b on a.relnamespace=b.oid
                where b.nspname = 'mssql'
    LOOP
        sql = 'create table run.' || tbl || '(like mssql.\"' || tbl || '\" including all)';
        EXECUTE sql;
    END LOOP;
END
\$\$;
"

psql "$CONNpg" <<_eof_
\set STOP_ON_ERROR on
 drop database if exists $DB;
 create database $DB;
_eof_

psql "$CONNpg" <<_eof_
\set STOP_ON_ERROR on
 alter database $DB set search_path=public,run,views,run_information_schema,mssql,mssql_information_schema,dbo;
_eof_


psql -1 "$CONN" <<_eof_
\set STOP_ON_ERROR on
\qecho *** Reset Environment *** ...

    create schema mssql;
    create schema run;
    create schema dbo;

    create extension if not exists "tds_fdw" with schema public;
    create extension if not exists "uuid-ossp" with schema public;

    create server mssql_svr
        foreign data wrapper tds_fdw
        options (servername '$MSSQL_SERVER',
                port '$MSSQL_PORT',
                database '$MSSQL_DB',
                tds_version '7.3',
                msg_handler 'notice');

    create user mapping for $USER
        server mssql_svr
        options (username '$MSSQL_USER', password '$MSSQL_PASS');
_eof_

psql -1 "$CONN" <<_eof_
\set STOP_ON_ERROR on
\qecho *** Now Importing MSSQL Tables as Foreign Tables *** ...

    import foreign schema dbo
        from server mssql_svr
        into mssql
        options (import_default 'true');

\qecho *** Now Creating Tables Based Upon Mssql Foreign Tables *** ...
    $SQL

_eof_


echo "$(date): DONE"
