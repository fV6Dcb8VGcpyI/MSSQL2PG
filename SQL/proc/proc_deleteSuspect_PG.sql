CREATE OR REPLACE FUNCTION dbo.deleteSuspect (
  IN suspectId_v integer
) RETURNS VOID AS
$$
BEGIN
  DELETE FROM suspectKeywords
    WHERE suspectId=suspectId_v;

  DELETE FROM suspects
    WHERE suspectId=suspectId_v;

  DELETE FROM keywords
    WHERE keywordId NOT IN (SELECT DISTINCT keywordID FROM suspectKeywords);
END;
$$
STABLE
LANGUAGE plpgsql;
