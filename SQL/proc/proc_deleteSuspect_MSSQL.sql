CREATE PROCEDURE [dbo].[deleteSuspect]
@SuspectId int
AS

DECLARE @dependantCount int

BEGIN TRAN transtart

DELETE suspectKeywords
WHERE suspectId=@suspectId

DELETE suspects
WHERE suspectId=@suspectId

DELETE keywords
WHERE keywordId NOT IN (SELECT DISTINCT keywordID FROM suspectKeywords)

COMMIT TRAN transtart
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
