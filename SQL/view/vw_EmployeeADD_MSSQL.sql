create view [dbo].[vw_EmployeeADD] as
SELECT     employee.employee_id, BenPtr.class_id AS EmpClass, BenDesc.*
FROM         (employee INNER JOIN
                      ben02 AS BenPtr ON employee.group_id = BenPtr.group_id AND employee.class_id = BenPtr.class_id) INNER JOIN
                      ben02 AS BenDesc ON BenPtr.group_id = BenDesc.group_id AND BenPtr.same_class_id = BenDesc.class_id AND
                      BenPtr.option_id = BenDesc.option_id
WHERE     BenPtr.coverage = 2
UNION ALL
SELECT     employee.employee_id, ben02.class_id, ben02.*
FROM         employee INNER JOIN
                      ben02 ON employee.group_id = ben02.group_id AND employee.class_id = ben02.class_id
WHERE     ben02.coverage <> 2
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
