CREATE OR REPLACE FUNCTION dbo.funct_DmL_employee_deps()
RETURNS TRIGGER
AS
$$
DECLARE
    mytablechange varchar(64) := TG_ARGV[0];
    mychangetype char;
    myrow record;
BEGIN
    if (TG_OP = 'UPDATE') then
        mychangetype = 'C';
        myrow = NEW;
    elsif  (TG_OP = 'INSERT') then
        mychangetype = 'A';
        myrow = NEW;
    elsif  (TG_OP = 'DELETE') then
        mychangetype = 'T';
        myrow = OLD;
    end if;

    insert into changes (username,cdate,tablechange,employee_id,dep_num,changetype)
        values(user, now(), mytablechange, myrow.employee_id, myrow.dep_num, mychangetype);

    return NEW;
END;
$$
LANGUAGE plpgsql;
