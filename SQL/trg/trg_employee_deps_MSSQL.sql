CREATE TRIGGER trg_employee ON dbo.employee
FOR UPDATE
NOT FOR REPLICATION
AS
IF EXISTS
(SELECT employee.employee_id
FROM (group_summary INNER JOIN employee
ON group_summary.group_id = employee.group_id)
INNER JOIN inserted
ON employee.employee_id = inserted.employee_id
WHERE group_summary.status = 'A')
BEGIN
        DECLARE @type CHAR(1)
        IF UPDATE (earnings) SET @type = 'S' ELSE SET @type = 'C'
        INSERT changes (username,cdate,tablechange,group_id,employee_id,class_id,changetype)
        SELECT system_user,getdate(),'Employee',inserted.group_id,inserted.employee_id,inserted.class_id,@type
        FROM inserted
END
