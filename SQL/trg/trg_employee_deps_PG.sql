DROP TRIGGER IF EXISTS trg_employee_deps ON run.employee_deps;

CREATE TRIGGER trg_employee_deps
    AFTER INSERT OR UPDATE ON run.employee_deps
    FOR EACH ROW
    EXECUTE PROCEDURE dbo.funct_DML_employee_deps('Dependent');
