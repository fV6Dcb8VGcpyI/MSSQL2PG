CREATE OR REPLACE FUNCTION dbo.fn_AffiliateIDs (
  IN pGroup_ID  integer,
 OUT AffiliateList varchar(500)
) AS
$$
BEGIN
  select string_agg(agroup_id::text, ',')
    into AffiliateList
    from group_agroups
    where group_id = pGroup_ID;
END;
$$
IMMUTABLE
LANGUAGE plpgsql;
