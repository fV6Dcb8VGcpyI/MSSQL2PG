CREATE OR REPLACE FUNCTION dbo.coOpPolNumSeries (
  IN pPolNum      varchar(30) default null,
 OUT polNumSeries varchar(30)
) AS
$$
BEGIN
  select trim(split_part(pPolNum,'-',1)) into polNumSeries::varchar(30);
END;
$$
IMMUTABLE
LANGUAGE plpgsql;
