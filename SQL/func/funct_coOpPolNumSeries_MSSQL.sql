CREATE FUNCTION [dbo].[coOpPolNumSeries]
(
@pPolNum AS VARCHAR(30)
)
RETURNS VARCHAR(30)
AS
BEGIN
	DECLARE @polNumSeries VARCHAR(30)
	IF CHARINDEX('-', @pPolNum) > 0
		SET @polNumSeries = SUBSTRING(@pPolNum, 1, CHARINDEX('-',@pPolNum)-1)
	ELSE
		SET @polNumSeries = @pPolNum
	RETURN @polNumSeries
END

