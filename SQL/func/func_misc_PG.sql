CREATE OR REPLACE FUNCTION public.newid(
 OUT uuid TEXT
) AS
$$
BEGIN
    uuid = upper(uuid_generate_v1mc()::text);
END;
$$
LANGUAGE PLPGSQL;

COMMENT ON FUNCTION public.newid() is 'MSSQL newid';
--------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.newsequentialid(
 OUT uuid TEXT
) AS
$$
BEGIN
    uuid = public.newid();
END;
$$
LANGUAGE PLPGSQL;

COMMENT ON FUNCTION public.newsequentialid() is 'MSSQL newsequentialid';
--------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.getdate (
    OUT getdate TIMESTAMP WITHOUT TIME ZONE
) AS
$$
BEGIN
    getdate = now()::timestamp(0) without time zone;
END;
$$
LANGUAGE PLPGSQL;

COMMENT ON FUNCTION public.getdate() is 'MSSQL getdate';
--------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.op_concat_varchar (text,text)
RETURNS text
AS
$$
BEGIN
    RETURN $1 || $2;
END;
$$
LANGUAGE PLPGSQL;

COMMENT ON FUNCTION public.op_concat_varchar(text,text) is 'MSSQL op_concat_varchar';

-- EXAMPLE: SELECT 'ABC' + 'DEF' + '123'
CREATE OPERATOR public.+ (
    procedure = public.op_concat_varchar,
    leftarg = text,
    rightarg = text
);

COMMENT ON OPERATOR public.+ (text,text) is 'MSSQL concatenate';
--------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.op_minus_timestamp_int (
    IN a timestamp with time zone,
    IN b integer,
   OUT c timestamp with time zone
) AS
$$
DECLARE
    SQL text;
BEGIN
    SQL = 'select ' || quote_literal($1) || '::timestamp with time zone' || ' - ' || quote_literal($2 || ' days' ) || '::interval';
    EXECUTE SQL INTO c USING a, b;
END;
$$
LANGUAGE PLPGSQL;

COMMENT ON FUNCTION op_minus_timestamp_int (timestamp with time zone,int) is 'MSSQL op_minus_timestamp_int';

-- EXAMPLE: SELECT now()-4; -- subtract 4 days
CREATE OPERATOR public.- (
    procedure = public.op_minus_timestamp_int,
    leftarg = timestamp with time zone,
    rightarg = integer
);

COMMENT ON OPERATOR public.- (timestamp with time zone,int) is 'MSSQL timestamp minux interval, days from op_minus_timestamp_int';
