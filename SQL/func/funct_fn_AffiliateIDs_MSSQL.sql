CREATE FUNCTION [dbo].[fn_AffiliateIDs]
(
	@pGroup_ID INT
)
RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @AffiliateList VARCHAR(500)

	SELECT @AffiliateList = COALESCE(@AffiliateList + ',', '') +
		CAST(agroup_id AS VARCHAR(10))
	FROM group_agroups
	WHERE group_id = @pGroup_ID

	RETURN @AffiliateList
END

